import 'package:demo_app_flutter/src/routes/routes.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp(route: 'home'));
}

class MyApp extends StatelessWidget {

  final String route;

  MyApp({this.route});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: route,
      routes: getApplicationRoute(),
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFF0443a5),
        primaryColorLight: Color(0xFF5e82ac),
        primaryColorDark: Color(0xFF143d6e),
        accentColor: Color(0xFF78a8d3),

        fontFamily: 'Ubuntu',

        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 96.0,),
          headline2: TextStyle(fontSize: 60.0,),
          headline3: TextStyle(fontSize: 48.0,),
          headline4: TextStyle(fontSize: 34.0,),
          headline5: TextStyle(fontSize: 24.0,),
          headline6: TextStyle(fontSize: 20.0,),
          subtitle1: TextStyle(fontSize: 16.0,),
          subtitle2: TextStyle(fontSize: 14.0,),
          bodyText1: TextStyle(fontSize: 16.0,),
          bodyText2: TextStyle(fontSize: 14.0,),
          button: TextStyle(fontSize: 14.0,),
          overline: TextStyle(fontSize: 12.0,),
          caption: TextStyle(fontSize: 10.0,),
        ),

        accentTextTheme: TextTheme(       
          headline1: TextStyle(fontSize: 96.0, color: Colors.white),
          headline2: TextStyle(fontSize: 60.0, color: Colors.white),
          headline3: TextStyle(fontSize: 48.0, color: Colors.white),
          headline4: TextStyle(fontSize: 34.0, color: Colors.white),
          headline5: TextStyle(fontSize: 24.0, color: Colors.white),
          headline6: TextStyle(fontSize: 20.0, color: Colors.white),
          subtitle1: TextStyle(fontSize: 16.0, color: Colors.white),
          subtitle2: TextStyle(fontSize: 14.0, color: Colors.white),
          bodyText1: TextStyle(fontSize: 16.0, color: Colors.white),
          bodyText2: TextStyle(fontSize: 14.0, color: Colors.white),
          button: TextStyle(fontSize: 14.0, color: Colors.white),
          overline: TextStyle(fontSize: 12.0, color: Colors.white),
          caption: TextStyle(fontSize: 10.0, color: Colors.white),
        ),

        buttonTheme: ButtonThemeData(
          buttonColor: Theme.of(context).primaryColor,
          padding: EdgeInsets.all(10.0),
        ),

      ),
    );
  }
}