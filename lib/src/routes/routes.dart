import 'package:demo_app_flutter/src/pages/home_page.dart';
import 'package:demo_app_flutter/src/pages/product_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getApplicationRoute() {
  return <String, WidgetBuilder>{
    'home'                  : (BuildContext context) => HomePage(),
    'product'               : (BuildContext context) => ProductPage(),
  };
}