import 'package:demo_app_flutter/src/providers/http_provider.dart';
import 'package:flutter/material.dart';

class ProductPage extends StatefulWidget {
  ProductPage({Key key}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);
    return Scaffold(
      appBar: AppBar(
        title: Text('Productos', style: Theme.of(context).accentTextTheme.headline5,),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.black12
        ),
        child: FutureBuilder(
          future: HttpProvider.getProducts(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData && snapshot.data["result"] == "Ok") {
              return PageView(
                scrollDirection: Axis.horizontal,
                controller: controller,
                children: 
                  snapshot.data["products"].map<Widget>((e) {
                    return _itemProduct(e);
                  }).toList(),
              );
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  Widget _itemProduct(item) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Theme.of(context).primaryColor),
          borderRadius: BorderRadius.circular(20.0)
        ),
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 200.0,
                  height: 200.0,
                  margin: EdgeInsets.symmetric(horizontal: 20.0),
                  child: item["image"]!=null?Image.network(item["image"]):Container(),
                ),
                Text(item["name"], style: Theme.of(context).textTheme.headline3,),
                Text(item["description"], style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic),),
                SizedBox(height: 10.0,),
                Text('NIO ${item["price"]}', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Theme.of(context).primaryColor),),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  icon: Icon(Icons.shopping_cart_outlined),
                  onPressed: () => {},
                ),
                IconButton(
                  icon: Icon(Icons.shopping_bag_outlined),
                  onPressed: () => {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

}