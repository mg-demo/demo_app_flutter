import 'dart:convert';

import 'package:http/http.dart' as http;

class HttpProvider {

  static String url = 'http://192.168.1.2:8000/api';

  /***************************************************************************************************************************************************/

  static String buscarError(http.Response request) {

    var res = json.decode(request.body);

    if (request.statusCode!=200) {
      if (request.statusCode==401) {
        return "login";
      }
      return "${res["result"].toString()} ${res["message"].toString()}";
    }

    if (res["result"].toString() == 'Error') {
      return res["message"].toString();
    }

    if (res["result"].toString() == 'Error') {
      return res["message"].toString();
    }

    return "";

  }

  static Map<String, String> myHeaders() {
    return {
      "Accept" : "application/json;charset=UTF-8",
      "Content-Type" : "application/json;charset=UTF-8",
      "Transfer-Encoding" : "chunked",
    };
  }

  /***************************************************************************************************************************************************/

  static Future<Map<String, dynamic>> getProducts() async {
    try{
      var uri = Uri.parse('$url/products');
      final request = await http.get(uri, headers: myHeaders()).timeout(const Duration(seconds: 30));
 
      return {
        "message" : buscarError(request),
        "result" : json.decode(request.body)["result"],
        "products" : json.decode(request.body)["products"],
      };
    }catch(e){
      return {
        "message" : e.toString(),
        "result" : "Error",
      };
    }
  }

}